module Examples.AnimatedTree exposing
    ( Model
    , Msg
    , init
    , main
    , subscriptions
    , ui
    , update
    )

import Browser
import Browser.Events
import Element exposing (Element)
import Element.Input as Input
import Examples.Tree
import FeatherIcons
import Html exposing (Html)


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


minAge =
    0


maxAge =
    7


type alias Model =
    { age : Float
    , play : Bool
    }


type alias Flags =
    ()


init : Flags -> ( Model, Cmd msg )
init flags =
    ( { age = 0.0
      , play = False
      }
    , Cmd.none
    )


ui : Examples.Tree.Config -> Model -> Element Msg
ui config model =
    let
        tree =
            { config | axiom = { axiom | age = model.age } }

        axiom =
            config.axiom

        controls =
            Element.row
                [ Element.spacing 12
                ]
                [ rewindButton
                , if model.age == maxAge then
                    resetButton

                  else if model.play then
                    pauseButton

                  else
                    playButton
                , forwardButton
                ]

        playButton =
            Input.button []
                { onPress = Just Play
                , label =
                    FeatherIcons.play
                        |> FeatherIcons.toHtml []
                        |> Element.html
                }

        pauseButton =
            Input.button []
                { onPress = Just Pause
                , label =
                    FeatherIcons.pause
                        |> FeatherIcons.toHtml []
                        |> Element.html
                }

        resetButton =
            Input.button []
                { onPress = Just Reset
                , label =
                    FeatherIcons.refreshCw
                        |> FeatherIcons.toHtml []
                        |> Element.html
                }

        rewindButton =
            Input.button []
                { onPress = Just Rewind
                , label =
                    FeatherIcons.rewind
                        |> FeatherIcons.toHtml []
                        |> Element.html
                }

        forwardButton =
            Input.button []
                { onPress = Just Forward
                , label =
                    FeatherIcons.fastForward
                        |> FeatherIcons.toHtml []
                        |> Element.html
                }
    in
    Examples.Tree.ui tree
        |> Element.el
            [ Element.height (Element.maximum 400 Element.fill)
            , Element.width Element.fill
            , Element.inFront
                (Element.el
                    [ Element.alpha
                        (if model.play then
                            0

                         else
                            0.3
                        )
                    , Element.mouseOver [ Element.alpha 0.5 ]
                    , Element.width Element.fill
                    , Element.height Element.fill
                    ]
                    (Element.el
                        [ Element.centerX
                        , Element.alignBottom
                        , Element.padding 40
                        ]
                        controls
                    )
                )
            ]


type Msg
    = Play
    | Pause
    | Animate Float
    | Reset
    | Rewind
    | Forward


view : Model -> Html Msg
view model =
    model
        |> ui Examples.Tree.defaults
        |> Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Play ->
            ( { model | play = True }
            , Cmd.none
            )

        Pause ->
            ( { model | play = False }
            , Cmd.none
            )

        Animate delta ->
            let
                progress =
                    min 32 delta / 1000
            in
            ( { model
                | age = grow progress model
                , play = model.age < maxAge
              }
            , Cmd.none
            )

        Reset ->
            ( { model
                | age = minAge
                , play = True
              }
            , Cmd.none
            )

        Rewind ->
            ( { model
                | age = grow -0.2 model
                , play = False
              }
            , Cmd.none
            )

        Forward ->
            ( { model
                | age = grow 0.2 model
                , play = False
              }
            , Cmd.none
            )


grow progress model =
    (model.age + progress)
        |> min maxAge
        |> max minAge


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.play then
        Browser.Events.onAnimationFrameDelta Animate

    else
        Sub.none

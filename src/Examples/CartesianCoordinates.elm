module Examples.CartesianCoordinates exposing
    ( Config
    , Model
    , Msg
    , init
    , main
    , subscriptions
    , ui
    , update
    , view
    )

import Browser
import Browser.Events
import CartesianPlane exposing (graph)
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import Html exposing (Html)
import Spring exposing (Spring)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Transformations exposing (Transformation)


type alias Config =
    { minX : Float
    , minY : Float
    , maxX : Float
    , maxY : Float
    }


defaults : Config
defaults =
    { minX = -150
    , minY = -150
    , maxX = 150
    , maxY = 150
    }


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { x : Spring
    , y : Spring
    }


type Msg
    = SetX Float
    | SetY Float
    | Animate Float


type alias Flags =
    ()


init : Flags -> ( Model, Cmd Msg )
init () =
    ( { x = Spring.create { strength = 20, dampness = 5 }
      , y = Spring.create { strength = 20, dampness = 5 }
      }
    , Cmd.none
    )


view : Model -> Html Msg
view model =
    model
        |> ui defaults
        |> Element.el
            [ Element.width (Element.maximum 600 Element.fill)
            , Element.centerX
            ]
        |> Element.layout
            [ Element.height Element.fill
            , Element.width Element.fill
            ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetX x ->
            ( { model | x = Spring.setTarget x model.x }
            , Cmd.none
            )

        SetY y ->
            ( { model | y = Spring.setTarget y model.y }
            , Cmd.none
            )

        Animate delta ->
            ( { model
                | x = Spring.jumpTo (Spring.target model.x) model.x
                , y = Spring.jumpTo (Spring.target model.y) model.y
              }
            , Cmd.none
            )


subscriptions model =
    if Spring.atRest model.x && Spring.atRest model.y then
        Sub.none

    else
        Browser.Events.onAnimationFrameDelta Animate


ui : Config -> Model -> Element Msg
ui config model =
    let
        graph =
            [ circle
                [ Transformations.Translate
                    (Spring.value model.x)
                    (Spring.value model.y)
                    |> Transformations.toString
                    |> Svg.Attributes.transform
                , r "2"
                , fill "magenta"
                ]
                []
            , text_
                [ Transformations.Translate
                    (5 + Spring.value model.x)
                    (-5 + Spring.value model.y)
                    |> Transformations.toString
                    |> Svg.Attributes.transform
                , fontSize "6"
                , fontFamily "Source Code Pro, monospace"
                , fill "gray"
                , dominantBaseline "central"
                ]
                [ text coordinates ]
            ]
                |> CartesianPlane.graph config
                |> Element.html
                |> Element.el
                    [ Element.height Element.fill
                    , Element.width Element.fill
                    ]

        coordinates =
            "{ x = "
                ++ (model.x |> Spring.target |> String.fromFloat)
                ++ ", y = "
                ++ (model.y |> Spring.target |> String.fromFloat)
                ++ "}"
    in
    Element.column
        [ Element.width Element.fill
        , Element.centerX
        , Element.spacing 30
        , Element.padding 30
        ]
        [ graph
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = SetX
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("x = " ++ String.fromFloat (Spring.target model.x))
            , min = config.minX
            , max = config.maxX
            , value = Spring.target model.x
            , thumb = Input.defaultThumb
            , step = Just 0.01
            }
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = SetY
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("y = " ++ String.fromFloat (Spring.target model.y))
            , min = config.minY
            , max = config.maxY
            , value = Spring.target model.y
            , thumb = Input.defaultThumb
            , step = Just 0.01
            }
        ]

module Examples.LineTypedTransformations exposing (main)

import Element
import Svg
import Svg.Attributes


main =
    Element.layout
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        (Element.html
            (Svg.svg
                [ Svg.Attributes.viewBox "-100 -100 200 200" ]
                [ Svg.line
                    [ Svg.Attributes.x2 "1"
                    , Svg.Attributes.stroke "black"
                    , transform
                        [ Rotate 30
                        , Scale 100 1
                        ]
                    ]
                    []
                ]
            )
        )


type Transformation
    = Identity
    | Scale Float Float
    | Translate Float Float
    | Rotate Float


transform transformations =
    let
        toString : Transformation -> String
        toString transformation =
            case transformation of
                Identity ->
                    ""

                Scale x y ->
                    "scale("
                        ++ String.fromFloat x
                        ++ ", "
                        ++ String.fromFloat y
                        ++ ")"

                Translate x y ->
                    "translate("
                        ++ String.fromFloat x
                        ++ ", "
                        ++ String.fromFloat y
                        ++ ")"

                Rotate angle ->
                    "rotate("
                        ++ String.fromFloat angle
                        ++ ")"
    in
    transformations
        |> List.map toString
        |> String.join " "
        |> Svg.Attributes.transform

module Examples.Circle exposing (Config, defaults, main, ui)

import Arc2d
import Array
import Element exposing (Element)
import Geometry.Svg
import Html exposing (Html)
import List.Extra as List
import Maybe.Extra as Maybe
import Point2d
import Protractor
import Svg exposing (Svg)
import Svg.Attributes
import Transformations exposing (Transformation(..))


type alias Config =
    { dots : Int -- Number of dots
    , circle : String -- Dash array
    , center : String -- Color
    , radius : Float -- The radius of the big circle
    , radi : String -- Dash array
    , protractor : Bool -- Display protractor?
    , scatter : Bool -- Are doot evenly distributed or scattered
    }


defaults : Config
defaults =
    { dots = 5
    , circle = "0 1"
    , center = "none"
    , radius = 80
    , radi = "0 1"
    , protractor = True
    , scatter = False
    }



{- TODO:
   - Display radius (text)
-}


main : Html.Html msg
main =
    ui defaults
        |> Element.el
            [ Element.width (Element.maximum 600 Element.fill)
            , Element.centerX
            ]
        |> Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            ]


palette : List String
palette =
    [ "skyblue"
    , "orange"
    , "red"
    , "lime"
    , "maroon"
    ]


ui : Config -> Element msg
ui config =
    let
        shapes =
            [ dots
            , center
            , circle
            , radi
            , protractor
            ]

        protractor =
            case config.protractor of
                True ->
                    Protractor.protractor
                        { radius = config.radius * 0.6
                        , strokeWidth = 0.5
                        }

                False ->
                    Svg.g
                        []
                        []

        pairs : List a -> List ( a, a )
        pairs list =
            case list of
                [] ->
                    []

                [ one ] ->
                    []

                one :: two :: rest ->
                    ( one, two ) :: pairs (two :: rest)

        dots =
            angles
                |> List.indexedMap
                    (\index angle ->
                        dot angle (color index)
                    )
                |> Svg.g []

        center =
            Svg.circle
                [ Svg.Attributes.r "2"
                , Svg.Attributes.fill config.center
                ]
                []

        circle =
            Svg.circle
                [ Svg.Attributes.r <| String.fromFloat config.radius
                , Svg.Attributes.stroke "silver"
                , Svg.Attributes.strokeWidth "1"
                , Svg.Attributes.strokeDasharray config.circle
                , Svg.Attributes.fill "none"
                ]
                []

        radi =
            angles
                |> List.map radius
                |> Svg.g []

        angles : List Float
        angles =
            if config.scatter then
                -- TODO: Generate a list of numbers [ 0, 5, 10, 15, ... 355 ] and use Random.Extra.List.shuffle and List.take config.dots to take a random sample.
                [ 5, 35, 85, 100, 260, 340, 300, 95, 180, 220 ]
                    |> List.take config.dots

            else
                config.dots
                    |> List.range 1
                    |> List.map
                        (\index ->
                            360 * toFloat index / toFloat config.dots
                        )

        dot : Float -> String -> Svg msg
        dot angle fill =
            Svg.circle
                [ Svg.Attributes.r "10"
                , Svg.Attributes.cx "0"
                , Svg.Attributes.cy "0"
                , Svg.Attributes.fill fill
                , [ Rotate angle
                  , Translate config.radius 0
                  ]
                    |> List.map Transformations.toString
                    |> String.join " "
                    |> Svg.Attributes.transform
                ]
                []

        color : Int -> String
        color index =
            palette
                |> Array.fromList
                |> Array.get (modBy (List.length palette) index)
                |> Maybe.withDefault "black"

        radius angle =
            Svg.line
                [ Svg.Attributes.x1 "0"
                , Svg.Attributes.y1 "0"
                , Svg.Attributes.x2 <| String.fromFloat config.radius
                , Svg.Attributes.y2 "0"
                , Svg.Attributes.stroke "pink"
                , Svg.Attributes.strokeDasharray config.radi
                , Rotate angle
                    |> Transformations.toString
                    |> Svg.Attributes.transform
                ]
                []
    in
    shapes
        |> Svg.svg
            [ Svg.Attributes.viewBox "-100 -100 200 200"
            ]
        |> Element.html

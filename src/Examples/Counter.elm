module Examples.Counter exposing
    ( Model
    , Msg
    , init
    , main
    , ui
    , update
    )

import Browser
import Element exposing (Element)
import Element.Border as Border
import Element.Input as Input
import Html exposing (Html)


main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


type alias Model =
    Int


type Msg
    = Increment
    | Decrement


init =
    0


view : Model -> Html Msg
view model =
    model
        |> ui
        |> Element.layout []


ui : Model -> Element Msg
ui model =
    Element.row
        [ Element.padding 10
        , Element.spacing 10
        ]
        [ Input.button []
            { onPress = Just Decrement
            , label = Element.text "-"
            }
        , model
            |> String.fromInt
            |> Element.text
        , Input.button []
            { onPress = Just Increment
            , label = Element.text "+"
            }
        ]


update msg model =
    case msg of
        Increment ->
            model + 1

        Decrement ->
            model - 1

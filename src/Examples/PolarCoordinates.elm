module Examples.PolarCoordinates exposing
    ( Model
    , Msg
    , init
    , main
    , ui
    , update
    , view
    )

import Browser
import CartesianPlane
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import Html exposing (Html)
import Svg exposing (..)
import Svg.Attributes exposing (..)


main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


type alias Model =
    { angle : Float
    , radius : Float
    }


type Msg
    = SetAngle Float
    | SetRadius Float


init : Model
init =
    { angle = 0, radius = 75 }


view : Model -> Html Msg
view model =
    Element.layout
        [ Element.height Element.fill
        , Element.width Element.fill
        ]
        (ui model)


ui : Model -> Element Msg
ui model =
    let
        point =
            cartesian model
    in
    Element.column
        [ Element.width Element.fill
        , Element.centerX
        , Element.spacing 30
        , Element.padding 30
        ]
        [ Element.el
            [ Element.height Element.fill
            , Element.width Element.fill
            ]
          <|
            Element.html <|
                CartesianPlane.graph
                    { minX = -150
                    , minY = -150
                    , maxX = 150
                    , maxY = 150
                    }
                    [ circle
                        [ cx <| String.fromFloat point.x
                        , cy <| String.fromFloat point.y
                        , r "2"
                        , fill "magenta"
                        ]
                        []
                    , line
                        [ x1 "0"
                        , x2 <| String.fromFloat point.x
                        , y1 "0"
                        , y2 <| String.fromFloat point.y
                        , stroke "magenta"
                        , strokeWidth "0.5"
                        ]
                        []
                    , text_
                        [ x <| String.fromFloat (point.x + 5)
                        , y <| String.fromFloat (point.y + 5)
                        , Svg.Attributes.fontSize "10pt"
                        ]
                        [ text <|
                            "( "
                                ++ (point.x
                                        |> round
                                        |> String.fromInt
                                   )
                                ++ " , "
                                ++ (point.x
                                        |> round
                                        |> String.fromInt
                                   )
                                ++ " )"
                        ]
                    ]
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = SetAngle
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("angle value: " ++ String.fromFloat model.angle)
            , min = 0
            , max = 360
            , value = model.angle
            , thumb = Input.defaultThumb
            , step = Just 1
            }
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = SetRadius
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("radius value: " ++ String.fromFloat model.radius)
            , min = 0
            , max = 150
            , value = model.radius
            , thumb = Input.defaultThumb
            , step = Just 0.01
            }
        ]


update : Msg -> Model -> Model
update msg model =
    case msg of
        SetAngle angle ->
            { model | angle = angle }

        SetRadius radius ->
            { model | radius = radius }


cartesian : { angle : Float, radius : Float } -> { x : Float, y : Float }
cartesian model =
    { x = model.radius * cos (degrees model.angle)
    , y = model.radius * sin (degrees model.angle)
    }

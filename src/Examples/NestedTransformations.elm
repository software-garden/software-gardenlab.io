module Examples.NestedTransformations exposing
    ( Model
    , Msg
    , init
    , main
    , ui
    , update
    )

import Array exposing (Array)
import Basics.Extra exposing (..)
import Browser
import Browser.Events
import CartesianPlane
import Dict.Any as Dict exposing (AnyDict)
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Geometry.Svg
import Html exposing (Html)
import Json.Decode exposing (Decoder)
import LineSegment2d
import List.Extra as List
import Point2d
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Transformations exposing (Transformation(..))


main : Program () Model Msg
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


type alias Model =
    AnyDict String Group (Array Transformation)


type Group
    = Pink
    | Green


type Msg
    = Msg Group GroupMsg


type GroupMsg
    = AddTransformation Transformation
    | DeleteTransformation Int
    | SetTransformation Int Transformation


init : Model
init =
    Dict.empty groupName
        |> Dict.insert Pink
            (Array.fromList
                [ Translate 0 0
                , Rotate 0
                , Scale 1 1
                ]
            )
        |> Dict.insert Green
            (Array.fromList
                [ Translate 0 0
                , Rotate 0
                , Scale 1 1
                ]
            )


view : Model -> Html Msg
view model =
    Element.layout
        [ Element.height Element.fill
        , Element.width Element.fill
        ]
        (ui model)


ui : Model -> Element Msg
ui model =
    let
        wrapper element =
            Element.column
                [ Element.width Element.fill
                , Element.centerX
                , Element.spacing 20
                ]
                [ Element.el
                    [ Element.width Element.fill
                    , Element.paddingEach { top = 0, right = 30, bottom = 0, left = 30 }
                    ]
                    (Element.html element)
                , Element.row [ Element.width Element.fill ]
                    (model
                        |> Dict.toList
                        |> List.map (uncurry controls)
                    )
                ]

        controls : Group -> Array Transformation -> Element Msg
        controls group transformations =
            Element.el
                [ Background.color (toColor group)
                , Element.padding 10
                , Element.width Element.fill
                ]
                (transformations
                    |> Array.toList
                    |> transformationsUI
                    |> Element.map (Msg group)
                )

        shape =
            Dict.foldr
                nestTransformationsGroup
                (g [] [])
                model

        nestTransformationsGroup : Group -> Array Transformation -> Svg Msg -> Svg Msg
        nestTransformationsGroup group transformations item =
            let
                color =
                    group
                        |> groupName
                        |> String.toLower
            in
            g
                [ transformations
                    |> Array.toList
                    |> List.map Transformations.toString
                    |> String.join " "
                    |> transform
                ]
                [ line
                    [ x1 "0"
                    , x2 "100"
                    , y1 "0"
                    , y2 "0"
                    , stroke color
                    , strokeWidth "1"
                    ]
                    []
                , circle [ cx "0", cy "0", r "2", fill color ] []
                , item
                ]
    in
    shape
        |> List.singleton
        |> CartesianPlane.graph
            { minX = -150
            , minY = -150
            , maxX = 150
            , maxY = 150
            }
        |> wrapper


transformationsUI : List Transformation -> Element GroupMsg
transformationsUI transformations =
    let
        addButtons =
            [ Element.text "Add transformation: "
            , Input.button []
                { onPress = Just (AddTransformation (Translate 0 0))
                , label = Element.text "Translate"
                }
            , Input.button []
                { onPress = Just (AddTransformation (Scale 1 1))
                , label = Element.text "Scale"
                }
            , Input.button []
                { onPress = Just (AddTransformation (Rotate 0))
                , label = Element.text "Rotate"
                }
            ]

        currentTrasformations =
            transformations
                |> List.indexedMap transformationUI
    in
    Element.column
        [ Element.width Element.fill
        , Font.size 12
        , Element.spacing 4
        ]
        [ Element.row
            [ Element.width Element.fill
            , Element.spacing 2
            ]
            addButtons
        , Element.column
            [ Element.width Element.fill
            , Element.spacing 10
            ]
            currentTrasformations
        ]


transformationUI : Int -> Transformation -> Element GroupMsg
transformationUI index transformation =
    let
        sliderBackground =
            Element.el
                [ Element.width Element.fill
                , Element.height (Element.px 2)
                , Element.centerY
                , Background.color <| Element.rgb 0.7 0.7 0.7
                , Border.rounded 2
                ]
                Element.none

        controls =
            case transformation of
                Identity ->
                    [ Element.text "Identity" ]

                Scale horizontal vertical ->
                    [ Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \x ->
                                SetTransformation index (Scale x vertical)
                        , label = Input.labelLeft [] (Element.text "horizontal")
                        , min = 0
                        , max = 10
                        , value = horizontal
                        , thumb = Input.defaultThumb
                        , step = Just 0.1
                        }
                    , Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \y ->
                                SetTransformation index (Scale horizontal y)
                        , label = Input.labelLeft [] (Element.text "vertical")
                        , min = 0
                        , max = 10
                        , value = vertical
                        , thumb = Input.defaultThumb
                        , step = Just 0.1
                        }
                    ]

                Translate x y ->
                    [ Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \value ->
                                SetTransformation index (Translate value y)
                        , label = Input.labelLeft [] (Element.text "x")
                        , min = -100
                        , max = 100
                        , value = x
                        , thumb = Input.defaultThumb
                        , step = Just 1
                        }
                    , Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \value ->
                                SetTransformation index (Translate x value)
                        , label = Input.labelLeft [] (Element.text "y")
                        , min = -100
                        , max = 100
                        , value = y
                        , thumb = Input.defaultThumb
                        , step = Just 1
                        }
                    ]

                Rotate angle ->
                    [ Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \value ->
                                SetTransformation index (Rotate value)
                        , label = Input.labelLeft [] (Element.text "angle")
                        , min = -360
                        , max = 360
                        , value = angle
                        , thumb = Input.defaultThumb
                        , step = Just 1
                        }
                    ]
    in
    Element.column
        [ Element.width Element.fill
        , Border.color (Element.rgb 0.9 0.9 0.9)
        , Border.width 3
        , Element.padding 5
        , Element.spacing 20
        ]
        [ Element.row [ Element.width Element.fill ]
            [ transformation
                |> List.singleton
                |> apply
                |> Element.text
                |> Element.el [ Element.width Element.fill ]
            , Input.button []
                { onPress = Just (DeleteTransformation index)
                , label = Element.el [] (Element.text "X")
                }
            ]
        , Element.column
            [ Element.width Element.fill
            , Element.spacing 20
            ]
            controls
        ]


update : Msg -> Model -> Model
update (Msg group msg) model =
    let
        transformations =
            model
                |> Dict.get group
                |> Maybe.withDefault Array.empty
                |> (\current ->
                        case msg of
                            AddTransformation transformation ->
                                Array.push transformation current

                            DeleteTransformation index ->
                                arrayDelete index current

                            SetTransformation index transformation ->
                                Array.set index transformation current
                   )

        arrayDelete index array =
            let
                end =
                    Array.length array

                front =
                    Array.slice 0 index array

                back =
                    Array.slice (index + 1) end array
            in
            Array.append front back
    in
    Dict.insert group transformations model


apply : List Transformation -> String
apply transformations =
    let
        toString : Transformation -> String
        toString transformation =
            case transformation of
                Identity ->
                    ""

                Scale x y ->
                    "scale("
                        ++ String.fromFloat x
                        ++ ", "
                        ++ String.fromFloat y
                        ++ ")"

                Translate x y ->
                    "translate("
                        ++ String.fromFloat x
                        ++ ", "
                        ++ String.fromFloat y
                        ++ ")"

                Rotate angle ->
                    "rotate("
                        ++ String.fromFloat angle
                        ++ ")"
    in
    transformations
        |> List.map toString
        |> String.join " "


groupName : Group -> String
groupName group =
    case group of
        Pink ->
            "Pink"

        Green ->
            "Green"


toColor : Group -> Element.Color
toColor group =
    case group of
        Pink ->
            Element.rgb 1 0.73 0.8

        Green ->
            Element.rgb 0.0 0.5 0.0


grid : List (Svg.Attribute msg) -> Float -> Float -> Svg msg
grid attributes unit size =
    let
        positiveValues =
            size
                / 2
                |> floor
                |> List.range 1
                |> List.map toFloat
                |> List.map ((*) unit)

        negativeValues =
            positiveValues
                |> List.map negate

        max =
            unit * size / 2

        min =
            negate max
    in
    ((positiveValues ++ negativeValues)
        |> List.map
            (\value ->
                [ ( Point2d.fromCoordinates ( value, min )
                  , Point2d.fromCoordinates ( value, max )
                  )
                , ( Point2d.fromCoordinates ( min, value )
                  , Point2d.fromCoordinates ( max, value )
                  )
                ]
            )
        |> List.concat
        |> List.map LineSegment2d.fromEndpoints
        |> List.map (Geometry.Svg.lineSegment2d attributes)
    )
        |> (::)
            (CartesianPlane.axes attributes
                { minX = size * unit * -0.5
                , minY = size * unit * -0.5
                , maxX = size * unit * 0.5
                , maxY = size * unit * 0.5
                }
            )
        |> g []

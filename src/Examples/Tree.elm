module Examples.Tree exposing
    ( Axiom
    , Config
    , Rule
    , Segment
    , defaults
    , main
    , ui
    )

import Dict exposing (Dict)
import Element exposing (Element)
import Svg exposing (Svg)
import Svg.Attributes
import Transformations


defaults : Config
defaults =
    { viewBox = "-400 -400 800 800"
    , axiom =
        { color = "purple"
        , rotation = -90
        , age = 7
        , growing = True
        }
    , rules =
        [ ( "purple"
          , [ { color = "green", rotation = 0.0 }
            , { color = "green", rotation = 30 }
            , { color = "green", rotation = -30 }
            ]
          )
        , ( "green"
          , [ { color = "brown", rotation = 0.0 }
            , { color = "brown", rotation = 30 }
            , { color = "brown", rotation = -30 }
            ]
          )
        , ( "brown"
          , [ { color = "green", rotation = 0.0 }
            , { color = "green", rotation = 30 }
            , { color = "green", rotation = -30 }
            ]
          )
        ]
    }


type alias Config =
    { viewBox : String
    , axiom : Axiom
    , rules : List Rule
    }


type alias Axiom =
    { color : String
    , rotation : Float
    , age : Float
    , growing : Bool
    }


type alias Rule =
    ( String, List Segment )


type alias Segment =
    { color : String, rotation : Float }


ui : Config -> Element msg
ui config =
    let
        dot age color rotation =
            Svg.circle
                [ Svg.Attributes.r
                    (String.fromFloat
                        (if config.axiom.growing then
                            age

                         else
                            10
                        )
                    )
                , Svg.Attributes.cx "0"
                , Svg.Attributes.cy "0"
                , Svg.Attributes.fill color
                , [ Transformations.Rotate rotation
                  , Transformations.Translate
                        (if config.axiom.growing then
                            age * 10

                         else
                            80
                        )
                        0
                  ]
                    |> List.map Transformations.toString
                    |> String.join " "
                    |> Svg.Attributes.transform
                ]
                []

        line age color rotation =
            Svg.line
                [ Svg.Attributes.strokeWidth "1"
                , Svg.Attributes.x1 "0"
                , Svg.Attributes.y1 "0"
                , Svg.Attributes.x2
                    (String.fromFloat
                        (if config.axiom.growing then
                            age * 10

                         else
                            80
                        )
                    )
                , Svg.Attributes.y2 "0"
                , Svg.Attributes.stroke color
                , Svg.Attributes.strokeWidth
                    (String.fromFloat
                        (if config.axiom.growing then
                            age

                         else
                            1
                        )
                    )
                , Transformations.Rotate rotation
                    |> Transformations.toString
                    |> Svg.Attributes.transform
                ]
                []

        rules : Dict String (List Segment)
        rules =
            Dict.empty
                |> Dict.insert "brown"
                    [ { color = "green", rotation = 20 }
                    , { color = "green", rotation = -20 }
                    , { color = "brown", rotation = 0 }
                    ]
                |> Dict.insert "green"
                    [ { color = "blue", rotation = -25 }
                    , { color = "blue", rotation = -5 }
                    , { color = "blue", rotation = 15 }
                    ]
                |> Dict.insert "blue"
                    [ { color = "brown", rotation = -25 }
                    , { color = "purple", rotation = -5 }
                    , { color = "purple", rotation = 25 }
                    ]

        segment : Float -> Segment -> Svg msg
        segment age { color, rotation } =
            if age <= 0 then
                Svg.g [] []

            else
                Svg.g []
                    [ config.rules
                        |> Dict.fromList
                        |> Dict.get color
                        |> Maybe.withDefault []
                        |> List.map (segment (age - 1))
                        |> Svg.g
                            [ [ Transformations.Rotate rotation
                              , Transformations.Translate
                                    (if config.axiom.growing then
                                        age * 10

                                     else
                                        80
                                    )
                                    0
                              ]
                                |> List.map Transformations.toString
                                |> String.join " "
                                |> Svg.Attributes.transform
                            ]
                    , dot age color rotation
                    , line age color rotation
                    ]
    in
    [ segment config.axiom.age
        { color = config.axiom.color
        , rotation = config.axiom.rotation
        }
    ]
        |> Svg.svg
            [ Svg.Attributes.height "100%"
            , Svg.Attributes.width "100%"
            , Svg.Attributes.style "background: none"
            , Svg.Attributes.viewBox config.viewBox
            ]
        |> Element.html


main =
    ui defaults
        |> Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            ]

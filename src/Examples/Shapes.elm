module Examples.Shapes exposing
    ( Config
    , Container
    , Shape(..)
    , main
    , ui
    )

import Axis2d
import Basics.Extra exposing (inDegrees)
import Circle2d
import Element exposing (Element)
import Geometry.Svg
import Html exposing (Html)
import Html.Attributes
import LineSegment2d
import Point2d exposing (Point2d)
import Svg exposing (Attribute, Svg)
import Svg.Attributes
import Transformations exposing (Transformation(..))


type alias Config =
    { container : Container
    , shapes : List Shape
    }


type alias Container =
    { background : String
    , viewBox : String
    , fill : Bool
    }


type Shape
    = Dot
        { transformations : List Transformation
        , radius : Float
        , color : String
        }
    | Line
        { length : Float
        , transformations : List Transformation
        , color : String
        }
    | Group
        { transformations : List Transformation
        }
        (List Shape)


defaults : Config
defaults =
    { container =
        { background = "none"
        , viewBox = "-300 -300 600 600"
        , fill = True
        }
    , shapes =
        [ Dot
            { transformations = []
            , radius = 10
            , color = "skyblue"
            }
        , Line
            { length = sqrt ((20 ^ 2) + (30 ^ 2))
            , transformations =
                [ atan2 20 30
                    |> inDegrees
                    |> Rotate
                ]
            , color = "green"
            }
        , Group
            { transformations =
                [ Rotate -45.0
                ]
            }
            [ Line
                { transformations = [ Translate 20 0 ]
                , length = 50
                , color = "orange"
                }
            , Dot
                { transformations = [ Translate 100 0 ]
                , radius = 20.0
                , color = "red"
                }
            , Group
                { transformations = [ Rotate -45, Translate 100 0 ]
                }
                [ Line
                    { length = 50
                    , transformations = [ Translate 20 0 ]
                    , color = "orange"
                    }
                , Dot
                    { transformations = [ Translate 100 0 ]
                    , radius = 20.0
                    , color = "red"
                    }
                ]
            ]
        ]
    }


main : Html msg
main =
    ui defaults
        |> Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            ]


ui : Config -> Element msg
ui { container, shapes } =
    let
        shape : Shape -> Svg msg
        shape s =
            case s of
                Dot { transformations, color, radius } ->
                    Point2d.origin
                        |> Circle2d.withRadius radius
                        |> Geometry.Svg.circle2d
                            [ Svg.Attributes.fill color
                            , transformations
                                |> List.map Transformations.toString
                                |> String.join " "
                                |> Svg.Attributes.transform
                            ]

                Line { length, transformations, color } ->
                    LineSegment2d.along Axis2d.x 0 length
                        |> Geometry.Svg.lineSegment2d
                            [ Svg.Attributes.stroke color
                            , Svg.Attributes.strokeWidth "1"
                            , transformations
                                |> List.map Transformations.toString
                                |> String.join " "
                                |> Svg.Attributes.transform
                            ]

                Group { transformations } ss ->
                    ss
                        |> List.map shape
                        |> Svg.g
                            [ transformations
                                |> List.map Transformations.toString
                                |> String.join " "
                                |> Svg.Attributes.transform
                            ]

        width =
            if container.fill then
                "100%"

            else
                "300"

        height =
            if container.fill then
                "100%"

            else
                "150"

        padding =
            if container.fill then
                "0"

            else
                "5px"
    in
    shapes
        |> List.map shape
        |> Svg.svg
            [ Svg.Attributes.viewBox container.viewBox
            , Html.Attributes.style "background" container.background
            , Svg.Attributes.width width
            , Svg.Attributes.height height
            , Html.Attributes.style "margin" padding
            ]
        |> Element.html

module Examples.ViewBox exposing
    ( Model
    , Msg
    , init
    , main
    , ui
    , update
    , view
    )

import Browser
import BrowserWindow
import CartesianPlane exposing (graph)
import Element
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import Html exposing (Html)
import Svg exposing (..)
import Svg.Attributes exposing (..)


main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


type alias Model =
    { left : Float
    , top : Float
    , width : Float
    , height : Float
    , preserveAspectRatio : Bool
    }


type Msg
    = Move Float Float
    | Resize Float Float
    | PreserveAspectRatio Bool


init : Model
init =
    { left = 0
    , top = 0
    , width = 50
    , height = 50
    , preserveAspectRatio = False
    }


view : Model -> Html.Html Msg
view model =
    let
        wrapper element =
            Element.el
                [ Element.width (Element.maximum 1200 Element.fill)
                , Element.height Element.fill
                , Element.centerX
                ]
                element
    in
    ui model
        |> wrapper
        |> Element.layout
            [ Element.height Element.fill
            , Element.width Element.fill
            ]


update : Msg -> Model -> Model
update msg model =
    case msg of
        Move left top ->
            { model
                | left = left
                , top = top
            }

        Resize width height ->
            { model
                | width = width
                , height = height
            }

        PreserveAspectRatio value ->
            { model | preserveAspectRatio = value }


ui model =
    let
        viewbox =
            svg
                [ viewBox "-100 -100 200 200"
                ]
                [ g [] world
                , rect
                    [ x (String.fromFloat model.left)
                    , y (String.fromFloat model.top)
                    , width (String.fromFloat model.width)
                    , height (String.fromFloat model.height)
                    , opacity "0.3"
                    , stroke "white"
                    , fill "pink"
                    ]
                    []
                ]
                |> Element.html
                |> Element.el
                    [ Element.centerX
                    , Element.centerY
                    , Element.width Element.fill
                    , Element.height Element.fill
                    ]

        viewport =
            svg
                [ [ model.left, model.top, model.width, model.height ]
                    |> List.map String.fromFloat
                    |> String.join " "
                    |> viewBox
                , preserveAspectRatio <|
                    case model.preserveAspectRatio of
                        True ->
                            "xMidYMid meet"

                        False ->
                            "none"
                , Svg.Attributes.style "width: 100%; height: 100%; flex-grow: 1; flex-basis: 0"
                ]
                [ g [] world
                , rect
                    [ x (String.fromFloat model.left)
                    , y (String.fromFloat model.top)
                    , width (String.fromFloat model.width)
                    , height (String.fromFloat model.height)
                    , opacity "0.3"
                    , stroke "white"
                    , fill "pink"
                    ]
                    []
                ]
                |> Element.html
                |> Element.el
                    [ Element.width Element.fill
                    , Element.height Element.fill
                    ]
                |> BrowserWindow.window
                    [ Element.centerX
                    , Element.centerY
                    , Element.width Element.fill
                    , Element.height Element.fill
                    ]
    in
    Element.column
        [ Element.width Element.fill
        , Element.height Element.fill
        , Element.spacing 30
        , Element.padding 30
        ]
        [ Element.row
            [ Element.width Element.fill
            , Element.height Element.fill
            , Element.centerX
            , Element.spacing 30
            ]
            [ viewbox
            , viewport
            ]
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = \left -> Move left model.top
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("left: " ++ String.fromFloat model.left)
            , min = -100
            , max = 100
            , value = model.left
            , thumb = Input.defaultThumb
            , step = Just 1
            }
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = \top -> Move model.left top
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("top: " ++ String.fromFloat model.top)
            , min = -100
            , max = 100
            , value = model.top
            , thumb = Input.defaultThumb
            , step = Just 1
            }
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = \width -> Resize width model.height
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("width: " ++ String.fromFloat model.width)
            , min = 1
            , max = 100
            , value = model.width
            , thumb = Input.defaultThumb
            , step = Just 1
            }
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = \height -> Resize model.width height
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("height: " ++ String.fromFloat model.height)
            , min = 1
            , max = 100
            , value = model.height
            , thumb = Input.defaultThumb
            , step = Just 1
            }
        , Input.checkbox []
            { checked = model.preserveAspectRatio
            , icon = Input.defaultCheckbox
            , label = Input.labelRight [] (Element.text "Preserve aspect ratio")
            , onChange = PreserveAspectRatio
            }
        ]


world : List (Svg Msg)
world =
    [ CartesianPlane.axes
        [ stroke "gray"
        , fill "gray"
        ]
        { minX = -100
        , minY = -100
        , maxX = 100
        , maxY = 100
        }
    , circle
        [ cx "0"
        , cy "0"
        , r "2"
        , fill "magenta"
        ]
        []
    ]

module Mark.Custom exposing
    ( colors
    , coupon
    , editor
    , elmRepl
    , emphasize
    , header
    , icon
    , image
    , link
    , list
    , monospace
    , note
    , paragraph
    , row
    , subtitle
    , text
    , title
    , window
    )

import BrowserWindow
import Dict
import Editor
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Extra as Element
import Element.Font as Font
import FeatherIcons exposing (icons)
import Html exposing (Html)
import Html.Attributes
import Mark
import Mark.Default
import Set


title : Mark.Block String
title =
    Mark.block "Title"
        identity
        Mark.string


header : Mark.Block (model -> Element msg)
header =
    Mark.Default.header
        [ Font.size 24
        , Font.underline
        , Element.paddingXY 0 24
        ]
        text


paragraph : Mark.Block (model -> Element msg)
paragraph =
    let
        render content model =
            Element.paragraph
                [ Element.paddingXY 0 10
                , Element.spacing 12
                , Element.css "hyphens" "auto"
                , Element.css "orphans" "3"
                , Font.justify
                ]
                (content model)
    in
    Mark.map
        render
        text


link =
    let
        render :
            String
            -> (model -> Element msg)
            -> model
            -> Element msg
        render url label model =
            Element.link
                [ Element.paddingXY 20 0
                , Font.size 24

                -- , Font.bold
                -- , Background.color colors.charcoal
                , Font.color colors.blue
                , Border.rounded 2
                , Element.mouseDown
                    [ Border.shadow
                        { offset = ( 0, 1 )
                        , size = 0
                        , blur = 3
                        , color = colors.gray
                        }
                    ]
                ]
                { url = url
                , label = label model
                }
    in
    Mark.record2 "Link"
        render
        (Mark.field "url" Mark.string)
        (Mark.field "label" paragraph)


coupon =
    let
        render :
            { icon : String
            , text : String
            , primary : Bool
            , href : String
            }
            -> model
            -> Element msg
        render config model =
            { url = config.href
            , label =
                [ FeatherIcons.icons
                    |> Dict.get config.icon
                    |> Maybe.map (FeatherIcons.toHtml [])
                    |> Maybe.map Element.html
                    |> Maybe.map (Element.el [ Element.paddingXY 10 0 ])
                    |> Maybe.withDefault Element.none
                , config.text
                    |> Element.text
                    |> List.singleton
                    |> Element.paragraph [ Font.center ]
                ]
                    |> Element.row
                        [ Element.centerX
                        ]
            }
                |> Element.link
                    [ Element.paddingXY 0 20
                    , Font.size 24
                    , Font.bold
                    , Background.color colors.white
                    , Element.width Element.fill

                    -- , Background.color colors.charcoal
                    , Font.color
                        (if config.primary then
                            colors.green

                         else
                            colors.charcoal
                        )
                    , Border.rounded 9
                    , Border.width 1
                    , Border.color
                        (if config.primary then
                            colors.green

                         else
                            colors.charcoal
                        )
                    , Border.width 1
                    , Border.dashed
                    , Element.mouseOver
                        [ Border.shadow
                            { offset = ( 0, 2 )
                            , size = 0
                            , blur = 3
                            , color = colors.charcoal
                            }
                        , Element.moveDown 5
                        , Element.moveLeft 3
                        , Element.rotate (degrees 1)
                        ]
                    ]
                |> Element.el
                    [ Background.color colors.gray
                    , Border.innerShadow
                        { offset = ( 0, 2 )
                        , size = 1
                        , blur = 3
                        , color = colors.charcoal
                        }
                    , Border.rounded 10
                    , Border.width 1
                    , Border.color
                        (if config.primary then
                            colors.green

                         else
                            colors.charcoal
                        )
                    , Border.width 1
                    , Border.dashed
                    ]
    in
    Mark.record4 "Coupon"
        (\icon_ text_ primary href -> { icon = icon_, text = text_, primary = primary, href = href })
        (Mark.field "icon" Mark.string)
        (Mark.field "text" Mark.string)
        (Mark.field "primary" Mark.bool)
        (Mark.field "href" Mark.string)
        |> Mark.map render


monospace : Mark.Block (model -> Element msg)
monospace =
    Mark.Default.monospace
        [ Element.width Element.fill
        , Element.padding 20
        , Font.size 16
        , Font.color colors.maroon
        , Font.family
            [ Font.typeface "Source Code Pro"
            , Font.monospace
            ]
        , Element.scrollbarY
        , Element.css "page-break-inside" "avoid"
        ]



-- TODO: Separate to Mark.Editor ?


type Annotation
    = None
    | Highlight Int Int Int Int
    | Fold Int Int


editor : Mark.Block (a -> Element msg)
editor =
    let
        render : Editor.Annotations -> String -> model -> Element msg
        render annotations_ contents model =
            Editor.editor Editor.defaults annotations_ contents

        annotations =
            Mark.block "Annotations"
                (extractAnnotations { highlights = [], folded = Set.empty })
                (Mark.manyOf [ none, highlight, fold ])

        extractAnnotations : Editor.Annotations -> List Annotation -> Editor.Annotations
        extractAnnotations ({ highlights, folded } as extracted) remaining =
            case remaining of
                [] ->
                    { extracted
                        | highlights = List.reverse highlights
                    }

                None :: rest ->
                    extractAnnotations
                        { highlights = highlights
                        , folded = folded
                        }
                        rest

                (Highlight top left width height) :: rest ->
                    extractAnnotations
                        { extracted
                            | highlights =
                                { top = top
                                , left = left
                                , width = width
                                , height = height
                                }
                                    :: highlights
                        }
                        rest

                (Fold start length) :: rest ->
                    extractAnnotations
                        { extracted
                            | folded =
                                (start + length - 1)
                                    |> List.range start
                                    |> Set.fromList
                                    |> Set.union folded
                        }
                        rest

        code : Mark.Block String
        code =
            Mark.block "Code"
                identity
                Mark.multiline

        none : Mark.Block Annotation
        none =
            -- A dummy annotation to satisfy manyOf block. See https://github.com/mdgriffith/elm-markup/issues/12
            Mark.stub "None" None

        highlight : Mark.Block Annotation
        highlight =
            Mark.record4 "Highlight"
                Highlight
                (Mark.field "top" Mark.int)
                (Mark.field "left" Mark.int)
                (Mark.field "width" Mark.int)
                (Mark.field "height" Mark.int)

        fold : Mark.Block Annotation
        fold =
            Mark.record2 "Fold"
                Fold
                (Mark.field "start" Mark.int)
                (Mark.field "length" Mark.int)
    in
    Mark.block "Editor"
        identity
        (Mark.startWith render
            annotations
            code
        )


elmRepl : Mark.Block (a -> Element msg)
elmRepl =
    let
        render content model =
            Element.column
                [ Border.width 3
                , Border.rounded 5
                , Border.color colors.charcoal
                , Background.color colors.charcoal
                , Element.css "page-break-inside" "avoid"
                , Font.family
                    [ Font.typeface "Source Code Pro"
                    , Font.monospace
                    ]
                , Element.css "page-break-inside" "avoid"
                ]
                [ Element.row
                    [ Element.width Element.fill
                    , Font.color colors.gray
                    ]
                    [ FeatherIcons.terminal
                        |> FeatherIcons.toHtml []
                        |> Element.html
                        |> Element.el
                            [ Element.height (Element.px 35)
                            , Element.padding 8
                            ]
                    , Element.el
                        [ Element.width Element.fill
                        , Font.size 16
                        , Font.family
                            [ Font.typeface "Source Code Pro"
                            , Font.monospace
                            ]
                        ]
                        (Element.text "Elm Repl")
                    ]
                , content
                    |> String.split "\n"
                    |> List.map Element.text
                    |> Element.column
                        [ Element.width Element.fill
                        , Element.padding 20
                        , Background.color colors.black
                        , Border.roundEach
                            { topLeft = 0
                            , topRight = 0
                            , bottomLeft = 3
                            , bottomRight = 3
                            }
                        , Font.size 14
                        , Font.family
                            [ Font.typeface "Source Code Pro"
                            , Font.monospace
                            ]
                        , Font.color colors.gray
                        , Element.scrollbarY
                        , Element.spacing 6
                        ]
                ]
    in
    Mark.block "ElmRepl"
        render
        Mark.multiline


window :
    Mark.Block (a -> Element msg)
    -> Mark.Block (a -> Element msg)
window block =
    let
        render child model =
            child model
                |> Element.el
                    [ Element.height (Element.px 400)
                    , Element.width Element.fill
                    ]
                |> BrowserWindow.window [ Element.css "page-break-inside" "avoid" ]
    in
    Mark.block "Window"
        render
        block


row :
    Mark.Block (List (a -> Element msg))
    -> Mark.Block (a -> Element msg)
row block =
    let
        render children model =
            children
                |> List.map (\child -> child model)
                |> List.map
                    (Element.el
                        [ Element.width Element.fill
                        , Border.width 1
                        , Border.rounded 5
                        , Border.color (Element.rgb 0.6 0.6 0.6)
                        ]
                    )
                |> Element.row [ Element.spacing 20 ]
    in
    Mark.block "Row"
        render
        block


note : Mark.Block (model -> Element msg)
note =
    let
        render elements model =
            elements
                |> List.map (\element -> element model)
                |> Element.textColumn
                    [ Element.padding 20
                    , Element.spacing 10
                    , Element.width Element.fill
                    , Border.width 1
                    , Border.color colors.gray
                    , Element.alpha 0.6
                    , Border.rounded 5
                    , Font.size 16
                    ]
    in
    Mark.block "Note"
        render
        (Mark.manyOf
            [ paragraph
            , header
            , list
            ]
        )


emphasize : Mark.Block (model -> Element msg)
emphasize =
    let
        render texts model =
            let
                elements =
                    texts
                        |> List.map (\fn -> fn model)

                heading =
                    List.head elements
                        |> Maybe.withDefault [ Element.none ]
                        |> Element.paragraph
                            [ Element.width Element.fill
                            , Font.bold
                            , Font.size 30
                            ]

                subheadings =
                    elements
                        |> List.drop 1
                        |> List.map
                            (Element.paragraph
                                [ Element.width Element.fill
                                ]
                            )
            in
            (heading :: subheadings)
                |> Element.textColumn
                    [ Font.center
                    , Element.paddingXY 0 40
                    , Element.width Element.fill
                    , Element.spacing 10
                    ]
    in
    Mark.block "Emphasize"
        render
        (Mark.manyOf [ text ])


subtitle =
    let
        render :
            String
            -> model
            -> Element msg
        render content model =
            content
                |> Element.text
                |> List.singleton
                |> Element.paragraph
                    [ Font.center
                    , Element.paddingXY 0 10
                    , Element.width Element.fill
                    , Element.spacing 10
                    , Font.bold
                    , content
                        |> String.toLower
                        |> String.words
                        |> String.join "-"
                        |> Html.Attributes.id
                        |> Element.htmlAttribute
                    ]
    in
    Mark.block "Subtitle"
        render
        Mark.string


image : Mark.Block (model -> Element msg)
image =
    Mark.Default.image
        [ Element.width Element.fill
        ]


list : Mark.Block (model -> Element msg)
list =
    Mark.Default.list
        { icon = Mark.Default.listIcon
        , style =
            \cursor ->
                case List.length cursor of
                    0 ->
                        -- top level element
                        [ Element.spacing 16 ]

                    1 ->
                        [ Element.spacing 16 ]

                    2 ->
                        [ Element.spacing 16 ]

                    _ ->
                        [ Element.spacing 8 ]
        }
        text


text : Mark.Block (model -> List (Element msg))
text =
    let
        defaultTextStyle =
            Mark.Default.defaultTextStyle
    in
    Mark.Default.textWith
        { defaultTextStyle
            | inlines =
                defaultTextStyle.inlines
                    ++ [ icon
                       , definition
                       , key
                       , drop
                       ]
            , code =
                [ Font.family
                    [ Font.typeface "Source Code Pro"
                    , Font.monospace
                    ]
                , Font.color colors.maroon
                , Element.paddingEach
                    { top = 3
                    , right = 5
                    , bottom = 2
                    , left = 5
                    }
                ]
        }


icon : Mark.Inline (model -> Element msg)
icon =
    Mark.inline "Icon"
        (\name model ->
            icons
                |> Dict.get name
                |> Maybe.map
                    (FeatherIcons.toHtml
                        [ Html.Attributes.style "height" "1em"
                        , Html.Attributes.style "width" "1em"
                        ]
                    )
                |> Maybe.map Element.html
                |> Maybe.withDefault
                    (Element.text
                        ("Icon not found: '" ++ name ++ "'")
                    )
                |> Element.el
                    [ Element.padding 4
                    , Element.css "vertical-align" "middle"
                    ]
        )
        |> Mark.inlineString "name"


key : Mark.Inline (model -> Element msg)
key =
    Mark.inline "Key"
        (\name model ->
            name
                |> List.map
                    (\chunk ->
                        Mark.Default.textFragment chunk model
                    )
                |> Element.row
                    [ Element.paddingEach
                        { top = 7
                        , bottom = 7
                        , right = 8
                        , left = 8
                        }
                    , Border.width 1
                    , Border.rounded 3
                    , Font.variant Font.smallCaps
                    , Font.size 12
                    , Element.moveUp 4
                    , Font.bold
                    , Background.color colors.charcoal
                    , Font.color colors.white
                    ]
        )
        |> Mark.inlineText


drop : Mark.Inline (model -> Element msg)
drop =
    Mark.inline "Drop"
        (\chunks model ->
            chunks
                |> List.map
                    (\chunk ->
                        Mark.Default.textFragment chunk model
                    )
                |> Element.row
                    [ Element.paddingEach
                        { top = 8
                        , bottom = 0
                        , right = 8
                        , left = 8
                        }
                    , Font.size 40
                    , Element.spacing 0
                    , Element.alignLeft
                    ]
        )
        |> Mark.inlineText


definition : Mark.Inline (model -> Element msg)
definition =
    Mark.inline "Definition"
        (\term definiens model ->
            term
                |> Element.text
                |> Element.el
                    [ Font.letterSpacing 1.1
                    , Font.shadow
                        { offset = ( 0, 0 )
                        , blur = 1
                        , color = colors.gray
                        }
                    , Element.htmlAttribute (Html.Attributes.title definiens)
                    ]
        )
        |> Mark.inlineString "term"
        |> Mark.inlineString "definiens"


colors =
    { maroon = Element.rgb 0.7 0 0
    , gray = Element.rgb 0.8 0.8 0.8
    , charcoal = Element.rgb 0.2 0.2 0.2
    , pink = Element.rgb 1 0.6 0.6
    , white = Element.rgb 1 1 1
    , black = Element.rgb 0 0 0
    , green = Element.rgb 0 0.6 0
    , blue = Element.rgb 0.07 0.52 0.81
    }

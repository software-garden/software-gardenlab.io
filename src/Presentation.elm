module Presentation exposing (Slide, markdown)

import Element exposing (Element)
import List.Extra as List
import Markdown


type alias Slide msg =
    List (Element msg)


markdown : String -> Element msg
markdown string =
    let
        dedentedString =
            string
                |> String.lines
                |> List.map dedent
                |> String.join "\n"

        dedent line =
            line
                |> String.toList
                |> List.indexedMap Tuple.pair
                |> List.dropWhile (\( index, character ) -> index < indentation && character == ' ')
                |> List.map Tuple.second
                |> String.fromList

        indentation =
            string
                |> String.lines
                |> List.filter (\line -> String.trim line /= "")
                |> List.head
                |> Maybe.withDefault ""
                |> String.toList
                |> List.findIndex ((/=) ' ')
                |> Maybe.withDefault 0
    in
    Markdown.toHtml [] dedentedString
        |> Element.html
        |> Element.el [ Element.centerX ]

module Editor exposing
    ( Annotations
    , Colors
    , Config
    , Region
    , defaults
    , editor
    )

import Basics.Extra exposing (uncurry)
import Dict exposing (Dict)
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Extra as Element
import Element.Font as Font
import Element.Input as Input
import FeatherIcons exposing (icons)
import List.Extra as List
import Set exposing (Set)
import Svg.Attributes


type alias Config =
    { path : String
    , colors : Colors
    }


type alias Colors =
    { annotations : Element.Color
    , background : Element.Color
    , primary : Element.Color
    , secondary : Element.Color
    , window : Element.Color
    }


defaults : Config
defaults =
    { path = "Elm"
    , colors =
        { primary = Element.rgb 0 0 0
        , secondary = Element.rgb 0.8 0.8 0.8
        , annotations = Element.rgb 0.7 0 0
        , background = Element.rgb 1 1 1
        , window = Element.rgb 0.2 0.2 0.2
        }
    }


type alias Region =
    { top : Int
    , left : Int
    , width : Int
    , height : Int
    }


type Fragment
    = Folded Int
    | Unfolded (List String)


type alias Annotations =
    { highlights : List Region
    , folded : Set Int
    }


editor : Config -> Annotations -> String -> Element msg
editor { path, colors } { highlights, folded } code =
    let
        topBar =
            Element.row
                [ Element.width Element.fill
                , Background.color colors.window
                , Font.color colors.secondary
                ]
                [ FeatherIcons.fileText
                    |> FeatherIcons.toHtml []
                    |> Element.html
                    |> Element.el
                        [ Element.height (Element.px 35)
                        , Element.padding 8
                        ]
                , Element.el
                    [ Element.width Element.fill
                    , Font.size 16
                    , Font.family
                        [ Font.typeface "Source Code Pro"
                        , Font.monospace
                        ]
                    ]
                    (Element.text path)
                ]

        contents : Element msg
        contents =
            code
                |> String.lines
                |> List.indexedMap Tuple.pair
                |> extractFragments folded []
                |> renderFragments 1 []
                |> Element.column
                    [ Element.width Element.fill
                    , Element.scrollbarX
                    ]

        renderFragments :
            Int
            -> List (Element msg)
            -> List Fragment
            -> List (Element msg)
        renderFragments start rendered fragments =
            case fragments of
                [] ->
                    -- We are done
                    List.reverse rendered

                (Folded length) :: rest ->
                    fold colors start (length + start - 1)
                        :: renderFragments (start + length) rendered rest

                (Unfolded lines) :: rest ->
                    unfolded colors highlighted start lines
                        :: renderFragments (start + List.length lines) rendered rest

        highlighted =
            highlights
                |> List.zip (List.map .top highlights)
                |> Dict.fromList
    in
    [ topBar
    , contents
    ]
        |> Element.column
            [ Border.width 3
            , Border.rounded 5
            , Border.color colors.window
            , Element.css "page-break-inside" "avoid"
            , Font.family
                [ Font.typeface "Source Code Pro"
                , Font.monospace
                ]
            , Element.css "page-break-inside" "avoid"
            , Element.width Element.fill
            ]


highlight : Element.Color -> Region -> Element msg
highlight color { top, left, width, height } =
    Element.row []
        [ " "
            |> String.repeat left
            |> Element.text
        , " "
            |> String.repeat width
            |> List.repeat height
            |> List.map Element.text
            |> List.map (Element.el [ Element.paddingXY 0 10 ])
            |> Element.column
                [ Element.behindContent
                    (Element.el
                        [ Element.width Element.fill
                        , Element.height Element.fill
                        , Border.color color
                        , -- Hand-drawn style borders. See https://codepen.io/tmrDevelops/pen/VeRvKX/
                          [ width, height ]
                            |> List.map ((*) 2)
                            |> List.map String.fromInt
                            |> List.map (\num -> num ++ "px")
                            |> List.repeat 2
                            |> List.concat
                            |> (\list -> [ list, List.reverse list ])
                            |> List.map (String.join " ")
                            |> String.join " / "
                            |> Element.css "border-radius"
                        , Border.width 5
                        , Element.scale 1.1
                        , Element.alpha 0.7
                        ]
                        Element.none
                    )
                , Element.css "pointer-events" "none"
                , Element.css "user-select" "none"
                , Element.css "-webkit-user-select" "none"
                , Element.css "-ms-user-select" "none"
                , Element.css "-webkit-touch-callout" "none"
                , Element.css "-o-user-select" "none"
                , Element.css "-moz-user-select" "none"
                ]
        ]


fold : Colors -> Int -> Int -> Element msg
fold colors start end =
    let
        lineNumbers =
            [ start, end ]
                |> List.map String.fromInt
                |> String.join " - "
                |> Element.text
                |> Element.el
                    [ Font.color colors.secondary
                    , Font.size 14
                    ]

        button =
            Input.button
                [ Font.size 10
                , Element.paddingXY 10 5
                , Background.color colors.secondary
                , Font.color colors.background
                , Border.rounded 3
                , Font.variant Font.smallCaps
                ]
                { onPress = Nothing
                , label = Element.text "unfold"
                }

        shadow =
            Element.el
                [ Element.width Element.fill
                , Element.height Element.fill
                , Border.innerShadow
                    { offset = ( 0, 3 )
                    , size = -6
                    , blur = 12
                    , color = colors.window
                    }
                ]
                Element.none
    in
    [ lineNumbers, button ]
        |> Element.row
            [ Element.spacing 20
            , Element.padding 10
            , Background.color colors.background
            , Element.width Element.fill
            ]
        |> Element.el
            [ Element.paddingXY 5 10
            , Element.width Element.fill
            , Element.paddingEach
                { top = 0
                , right = 1
                , bottom = 0
                , left = 1
                }
            , Background.color colors.window
            , Element.inFront shadow
            ]


unfolded : Colors -> Dict Int Region -> Int -> List String -> Element msg
unfolded colors highlighted start lines =
    lines
        |> List.indexedMap
            (\n loc ->
                Element.row []
                    [ Element.el
                        [ Font.color colors.secondary
                        , Font.extraLight
                        , Element.width (Element.px 40)
                        , Element.padding 10
                        , Font.alignRight
                        , Element.css "user-select" "none"
                        , Element.css "-webkit-user-select" "none"
                        , Element.css "-ms-user-select" "none"
                        , Element.css "-webkit-touch-callout" "none"
                        , Element.css "-o-user-select" "none"
                        , Element.css "-moz-user-select" "none"
                        ]
                        ((n + start)
                            |> String.fromInt
                            |> Element.text
                        )
                    , Element.el
                        [ Element.width Element.fill
                        , Element.padding 10
                        , highlighted
                            |> Dict.get (n + start)
                            |> Maybe.map (highlight colors.annotations)
                            |> Maybe.withDefault Element.none
                            |> Element.behindContent
                        ]
                        (Element.text loc)
                    ]
            )
        |> Element.column
            [ Element.width Element.fill
            , Font.size 16
            , Element.clipY
            ]


extractFragments :
    Set Int
    -> List Fragment
    -> List ( Int, String )
    -> List Fragment
extractFragments folded accumulated lines =
    case lines of
        [] ->
            -- We have reached the end of the code
            case accumulated of
                [] ->
                    -- It was empty. Add one empty line.
                    [ Unfolded [ "" ] ]

                (Folded n) :: _ ->
                    List.reverse accumulated

                (Unfolded linesReversed) :: previous ->
                    List.reverse
                        (Unfolded (List.reverse linesReversed)
                            :: previous
                        )

        ( n, line ) :: rest ->
            let
                isFolded =
                    Set.member (n + 1) folded
            in
            case accumulated of
                [] ->
                    -- It's the first fragment. Check if it's folded or not.
                    if isFolded then
                        extractFragments folded
                            [ Folded 1 ]
                            rest

                    else
                        extractFragments folded
                            [ Unfolded [ line ] ]
                            rest

                (Folded length) :: previous ->
                    if isFolded then
                        -- Continue the fold
                        let
                            this =
                                Folded (length + 1)
                        in
                        extractFragments folded
                            (this :: previous)
                            rest

                    else
                        -- Fold is finished. Start a new unfolded fragment.
                        let
                            this =
                                Folded length

                            next =
                                Unfolded [ line ]
                        in
                        extractFragments folded
                            (next :: this :: previous)
                            rest

                (Unfolded linesReversed) :: previous ->
                    if isFolded then
                        -- Unfolded fragment is finished. Reverse the lines and start a new fold.
                        let
                            this =
                                Unfolded (List.reverse linesReversed)

                            next =
                                Folded 1
                        in
                        extractFragments
                            folded
                            (next :: this :: previous)
                            rest

                    else
                        -- Continue the unfolded fragment
                        let
                            this =
                                Unfolded (line :: linesReversed)
                        in
                        extractFragments
                            folded
                            (this :: previous)
                            rest

module Examples exposing (Model, Msg(..), init, subscriptions, update)

import Examples.AnimatedTree
import Examples.CartesianCoordinates
import Examples.Circle
import Examples.Counter
import Examples.Gradient
import Examples.NestedTransformations
import Examples.PolarCoordinates
import Examples.RosetteTypedTransformations
import Examples.Shapes
import Examples.Spiral
import Examples.Transformations
import Examples.Tree
import Examples.ViewBox


type alias Model =
    { counter : Examples.Counter.Model
    , transformations : Examples.Transformations.Model
    , nestedTransformations : Examples.NestedTransformations.Model
    , cartesianCoordinates : Examples.CartesianCoordinates.Model
    , polarCoordinates : Examples.PolarCoordinates.Model
    , viewBox : Examples.ViewBox.Model
    , animatedTree : Examples.AnimatedTree.Model
    }


type Msg
    = CounterMsg Examples.Counter.Msg
    | TransformationsMsg Examples.Transformations.Msg
    | NestedTransformationsMsg Examples.NestedTransformations.Msg
    | CartesianCoordinatesMsg Examples.CartesianCoordinates.Msg
    | PolarCoordinatesMsg Examples.PolarCoordinates.Msg
    | ViewBoxMsg Examples.ViewBox.Msg
    | AnimatedTreeMsg Examples.AnimatedTree.Msg


init : ( Model, Cmd Msg )
init =
    let
        ( animatedTreeModel, animatedTreeCmd ) =
            Examples.AnimatedTree.init ()

        ( cartesianCoordinatesModel, cartesianCoordinatesCmd ) =
            Examples.CartesianCoordinates.init ()
    in
    ( { counter = Examples.Counter.init
      , transformations = Examples.Transformations.init
      , nestedTransformations = Examples.NestedTransformations.init
      , cartesianCoordinates = cartesianCoordinatesModel
      , polarCoordinates = Examples.PolarCoordinates.init
      , viewBox = Examples.ViewBox.init
      , animatedTree = animatedTreeModel
      }
    , [ animatedTreeCmd |> Cmd.map AnimatedTreeMsg
      , cartesianCoordinatesCmd |> Cmd.map CartesianCoordinatesMsg
      ]
        |> Cmd.batch
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        CounterMsg m ->
            ( { model | counter = Examples.Counter.update m model.counter }
            , Cmd.none
            )

        TransformationsMsg m ->
            ( { model
                | transformations =
                    Examples.Transformations.update m model.transformations
              }
            , Cmd.none
            )

        NestedTransformationsMsg m ->
            ( { model
                | nestedTransformations =
                    Examples.NestedTransformations.update m model.nestedTransformations
              }
            , Cmd.none
            )

        CartesianCoordinatesMsg m ->
            let
                ( cartesianCoordinatesModel, cartesianCoordinatesCmd ) =
                    Examples.CartesianCoordinates.update m model.cartesianCoordinates
            in
            ( { model
                | cartesianCoordinates =
                    cartesianCoordinatesModel
              }
            , cartesianCoordinatesCmd
                |> Cmd.map CartesianCoordinatesMsg
            )

        PolarCoordinatesMsg m ->
            ( { model
                | polarCoordinates =
                    Examples.PolarCoordinates.update m model.polarCoordinates
              }
            , Cmd.none
            )

        ViewBoxMsg m ->
            ( { model | viewBox = Examples.ViewBox.update m model.viewBox }, Cmd.none )

        AnimatedTreeMsg m ->
            let
                ( animatedTreeModel, animatedTreeMsg ) =
                    Examples.AnimatedTree.update m model.animatedTree
            in
            ( { model | animatedTree = animatedTreeModel }
            , animatedTreeMsg |> Cmd.map AnimatedTreeMsg
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    [ model.animatedTree
        |> Examples.AnimatedTree.subscriptions
        |> Sub.map AnimatedTreeMsg
    , model.cartesianCoordinates
        |> Examples.CartesianCoordinates.subscriptions
        |> Sub.map CartesianCoordinatesMsg
    ]
        |> Sub.batch

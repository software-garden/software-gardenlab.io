module CartesianPlane exposing (axes, graph)

import Direction2d
import Geometry.Svg
import Html exposing (Html)
import LineSegment2d
import Point2d exposing (Point2d)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Triangle2d
import Vector2d


type alias Config =
    { minX : Float
    , minY : Float
    , maxX : Float
    , maxY : Float
    }


graph : Config -> List (Svg msg) -> Html msg
graph config shapes =
    let
        size =
            Basics.max (config.maxX - config.minX) (config.maxY - config.minY)

        hairline =
            20 / size

        fontsize =
            100 / size

        background =
            axes
                [ stroke "gray"
                , fill "gray"
                ]
                config
    in
    svg
        [ [ config.minX
          , config.minY
          , config.maxX - config.minX
          , config.maxY - config.minY
          ]
            |> List.map String.fromFloat
            |> String.join " "
            |> viewBox
        , preserveAspectRatio "xMidYMid meet"
        , Svg.Attributes.width "100%"
        , Svg.Attributes.height "100%"
        , Svg.Attributes.style "width: 100%, height: 100%"
        ]
        (background :: shapes)


axes : List (Attribute msg) -> Config -> Svg msg
axes attributes config =
    let
        xAxis =
            { start = Point2d.fromCoordinates ( config.minX, 0 )
            , end = Point2d.fromCoordinates ( config.maxX, 0 )
            }

        yAxis =
            { start = Point2d.fromCoordinates ( 0, config.minY )
            , end = Point2d.fromCoordinates ( 0, config.maxY )
            }
    in
    g []
        [ arrow
            attributes
            xAxis.start
            xAxis.end
        , arrow
            attributes
            yAxis.start
            yAxis.end
        , label [ fontSize "8", color "gray" ]
            (xAxis.end
                |> Point2d.translateIn (Direction2d.fromAngle (degrees -135)) 10
            )
            "x"
        , label [ fontSize "8", color "gray" ]
            (yAxis.end
                |> Point2d.translateIn (Direction2d.fromAngle (degrees -45)) 10
            )
            "y"
        , label [ fontSize "8", color "gray" ]
            (Point2d.origin
                |> Point2d.translateIn (Direction2d.fromAngle (degrees 135)) 10
            )
            "O"
        ]


arrow : List (Svg.Attribute msg) -> Point2d -> Point2d -> Svg msg
arrow attributes start end =
    let
        origin =
            Point2d.fromCoordinates ( 0, 0 )

        direction =
            Direction2d.from start end

        triangle =
            Triangle2d.fromVertices
                ( Point2d.fromCoordinates ( 0, 0 )
                , Point2d.fromCoordinates ( -4, -2 )
                , Point2d.fromCoordinates ( -4, 2 )
                )

        vector =
            Vector2d.from origin end
    in
    case direction of
        Nothing ->
            g [] []

        Just dir ->
            g []
                [ triangle
                    |> Triangle2d.rotateAround origin (Direction2d.toAngle dir)
                    |> Triangle2d.translateBy vector
                    |> Geometry.Svg.triangle2d ([ strokeWidth "0" ] ++ attributes)
                , end
                    |> Point2d.translateIn dir -4
                    |> LineSegment2d.from start
                    |> Geometry.Svg.lineSegment2d attributes
                ]


label : List (Svg.Attribute msg) -> Point2d -> String -> Svg msg
label attributes center content =
    text_
        ([ x <| String.fromFloat (Point2d.xCoordinate center)
         , y <| String.fromFloat (Point2d.yCoordinate center)
         , dominantBaseline "central"
         , textAnchor "middle"
         ]
            ++ attributes
        )
        [ text content ]

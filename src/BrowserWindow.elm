module BrowserWindow exposing (main, window)

import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Html exposing (Html)


main : Html msg
main =
    Element.text "Hello World!"
        |> Element.el
            [ Element.width (Element.px 400)
            , Element.height (Element.px 400)
            ]
        |> window []
        |> Element.layout []


window : List (Element.Attribute msg) -> Element msg -> Element msg
window attributes content =
    let
        circle : Element.Color -> Element msg
        circle color =
            Char.fromCode 9679
                |> String.fromChar
                |> Element.text
                |> Element.el
                    [ color |> Font.color
                    , Font.size 35
                    , Element.width (Element.px 20)
                    , Element.height (Element.px 35)
                    , Font.center
                    ]
    in
    Element.el attributes <|
        Element.column
            [ Element.width Element.fill
            , Element.height Element.fill
            , Border.width 2
            , Border.rounded 6
            , Border.color <| Element.rgb 0.27 0.27 0.27
            , Background.color <| Element.rgb 0.27 0.27 0.27
            , Element.clip
            ]
            [ Element.row
                [ Element.height <| Element.px <| 23
                , Element.width Element.fill
                , Border.widthEach { bottom = 2, left = 0, top = 0, right = 0 }
                , Border.color <| Element.rgb 0.2 0.2 0.2
                , Element.paddingXY 10 15
                ]
                ([ ( 1, 0.4, 0.3 ), ( 1, 0.75, 0.18 ), ( 0.16, 0.79, 0.26 ) ]
                    |> List.map
                        (\( red, green, blue ) ->
                            circle (Element.rgb red green blue)
                        )
                )
            , Element.el
                [ Element.width Element.fill
                , Element.height Element.fill
                , Background.color <| Element.rgb 1 1 1
                ]
                content
            ]

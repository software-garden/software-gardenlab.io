---
presentation:
  enableSpeakerNotes: true
  theme: solarized.css
  height: 150%
---

<!-- slide -->

# After day 1

<!-- slide vertical -->

On day one we have created a program that ...


1. ... calculates the phase of the moon
1. ... displays a dot in the middle of the screen <!-- .element: class="fragment grow"  -->
1. ... makes the screen pink


<!-- slide vertical -->

Consider the following code:

```elm
Svg.svg
    [ Svg.Attributes.viewBox "-100 -100 200 200" ]
    [ Svg.circle
        [ Svg.Attributes.cx "300"
        , Svg.Attributes.cy "100"
        , Svg.Attributes.r "10"
        ]
        []
    ]
```

Where will the dot be?


1. Visible on the screen
1. To the left of the screen (invisible)
1. To the right of the screen (invisible) <!-- .element: class="fragment grow"  -->


<!-- slide vertical -->

What does it mean to format the code?


1. To make it work when previously it was broken.
1. To make it look good.  <!-- .element: class="fragment grow"  -->
1. To write it from scratch.
1. To take code created by somebody else and adapt it to our needs.


<!-- slide vertical -->

A way of describing a position of a point on flat a surface is called:

1. the origin
1. geography
1. SVG
1. cartesian coordinates system <!-- .element: class="fragment grow"  -->
1. X - Y system





<!-- slide vertical -->

In SVG, the greater the value of the y coordinate ...

1. ... the lower the point is on the screen. <!-- .element: class="fragment grow"  -->
1. ... the higher the point is on the screen.
1. ... the bigger the point is.


<!-- slide vertical -->

The value of `Svg.Attributes.r` affects:

1. rotation of a circle
1. roundness of a circle
1. size of a circle  <!-- .element: class="fragment grow"  -->


<!-- slide vertical -->

The value of `Svg.Attributes.viewBox` controls:

1. How big the screen is.
1. At what fragment of a surface we are looking.  <!-- .element: class="fragment grow"  -->
1. How big the SVG element is.
1. How big the viewport is.


<!-- slide vertical -->

What is the origin?

1. The first copy of a program.
1. A number representing the size of a dot.
1. A point in cartesian coordinates system.  <!-- .element: class="fragment grow"  -->


<!-- slide vertical -->

If the viewbox is

```elm
{ left = -200
, top = -200
, width = 300
, height = 200
}
```

then the point at the centre is:

1. `{ x = 0, y = 0 }`
1. `{ x = -50, y = -100 }` <!-- .element: class="fragment grow"  -->
1. `{ x = 150, y = 100 }`


<!-- slide vertical -->

To make sure that a given point is in the middle of the screen, we can:

1. Move the viewport
1. Move the viewbox <!-- .element: class="fragment grow"  -->
1. Move the screen
1. Move the point


<!-- slide vertical -->

```elm
main =
    Svg.svg
        [ Svg.Attributes.viewBox "-100 -100 200 200" ]
        [ Svg.rect
            [ Svg.Attributes.x "-10"
            , Svg.Attributes.y "-10"
            , Svg.Attributes.width "20"
            , Svg.Attributes.height "20"
            ]
            []
        ]
```

Which output will this program produce?

<div style="display: flex">
  <svg viewbox="-100 -100 200 200" style=" background: white; border: solid 1px black; border-radius: 10px; margin: 30px;" class="fragment grow">
    <rect x="-10" y="-10" width="20"  height="20">
  </svg>

  <svg viewbox="-100 -100 200 200" style=" background: white; border: solid 1px black; border-radius: 10px; margin: 30px;">
    <circle r="10">
  </svg>

  <svg viewbox="-100 -100 200 200" style=" background: white; border: solid 1px black; border-radius: 10px; margin: 30px;">
    <rect x="-15" y="-10" width="30"  height="20">
  </svg>
</div>


<!-- slide vertical -->

What is this code missing?

```elm
main =
    Svg.svg
        [ Svg.Attributes.viewBox "-100 -100 200 200"
        [ Svg.rect
            [ x "-10"
            , y "-10"
            , width "20"
            , height "20"
            ]
            []
        ]
```

1. A closing quotation mark
1. A closing square bracket <!-- .element: class="fragment grow"  -->
1. An equal sign


<!-- slide vertical -->

This fragment of code is broken:

```elm
Svg.svg
    [ Svg.Attributes.viewBox "-100 -100 200 200" ]
    [ Svg.circle [ Svg.Attributes.r: "10" ] [] ]
```

Which one is the correct version?

```elm
Svg.svg
    [ Svg.Attributes.viewBox = "-100 -100 200 200" ]
    [ Svg.circle [ Svg.Attributes.r = "10" ] [] ]
```

```elm
Svg.svg
    [ Svg.Attributes.viewBox: "-100 -100 200 200" ]
    [ Svg.circle [ Svg.Attributes.r: "10" ] [] ]
```

```elm { .fragment .grow}
Svg.svg
    [ Svg.Attributes.viewBox "-100 -100 200 200" ]
    [ Svg.circle [ Svg.Attributes.r "10" ] [] ]
```

<!-- slide vertical -->

## You are the best 👑


<!-- slide -->

# After day 2


<!-- slide vertical -->

> TODO: Flashcards for day 2

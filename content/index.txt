| Title
    Growing an Elm Tree

| Emphasize
    <>

    A Software Development Workshop for Non-Programmers
    by {Link|Software Garden|url=https://software.garden/}
    in Amsterdam and Utrecht, the Netherlands

Hello! We are running a workshop that will give you a glimpse into the way software is created. We think {Link|it's important|url=/motivation.html}.

During this *5 days workshop* (3 hours each day) you will learn to solve problems using a functional programming language.  Together, we will build a program that simulates growth of a tree, like this:

| Window
    | AnimatedTree
        | Axiom
            color = olive
            rotation = -90
            age = 8
            growing = True

        | Rule
            parent = olive

            children =
                | Child
                    color = olive
                    rotation = 5.0

                | Child
                    color = maroon
                    rotation = 40

                | Child
                    color = maroon
                    rotation = -40

        | Rule
            parent = maroon
            children =
                | Child
                    color = olive
                    rotation = 20
                | Child
                    color = olive
                    rotation = -20

Our workshop is intended for people with no prior experience in programming and doesn't require any technical knowledge. Everybody is welcome!


| Subtitle
    Next Round

Currently we don't have a fixed date for the next round yet. If you are interested to participate please get in touch and we will arrange a suitable schedule.


| Emphasize
    Say hello {Icon|name=phone} {Link|+31 638 216 166|url=tel:+31638216166}

    or drop us a note at {Link|software-garden@tad-lispy.com|url= mailto:software-garden@tad-lispy.com}


| Subtitle
    How Much Does It Cost?

The price for individual participants is *500<>€* (inc. 21% VAT). Utrecht University students enjoy a special discounted price of *300<>€* (inc. 21% VAT). We can also offer a discount for groups.


| Subtitle
    What Do I Get?

You will get 15 hours of training. Upon completion of the course your name, program that you will have created and link to your website (or LinkedIn profile etc) will be posted {Link|here|url=/test-run.html}.

Below is the material through which we will be going during the workshop. Only the first section is a required read before you come.

| Link
    url = /preparation.html
    label = Before the course begins

| Link
    url = /day-1.html
    label = Day 1 - Let's Make a Dot


| Link
    url = /day-2.html
    label = Day 2 - Let's Place the Dots in a Circle

| Link
    url = /day-3.html
    label = Day 3 - Connecting the Dots

| Link
    url = /day-4.html
    label = Day 4 - Let's Make a Tree

| Link
    url = /day-5.html
    label = Day 5 - Let's Make the Tree Grow


| Subtitle
    About us

*Tad Lispy*: I'm going to be your teacher during the workshop. I'm a co-founder of Software Garden and I've been working as a software developer for the best part of the last eight years. Before that I've been a lawyer.

I love the creativity of the software development and hope to share that passion with you.

*Fana* I'm a Clojure developer at {Link|AdGoji|url=https://www.adgoji.com/}. At the workshop I am going to be your teaching assistant. Previously I was teaching English in China and working as a marine biologist {Icon|name=anchor} in Eritrea, Italy, Finland, Azores and Spain.

*Sam Phillips* is a co-author of the workshop. He is an Elm developer at {Link|itravel|url=https://www.itravel.de/} in Cologne, Germany.

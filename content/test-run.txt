| Title
    Former Students

In collaboration with {Link|Career Services|url=https://students.uu.nl/en/careerservices} of Utrecht University we offered a one time, free workshop for 9 students from March 4 - March 8. Here are our students with their animated tree:

| Header
    Sarah Warnau

Student of Environmental Sciences, Utrecht University

See {Link|Sarah's program|url=https://ellie-app.com/4WbyTFpZr6Da1} and {Link|LinkedIn profile|url=https://www.linkedin.com/in/sarah-warnau/}

| Header
    Maarten Zwaal

Student of Linguistics, Utrecht University

See {Link|Maarten's program|url=https://ellie-app.com/4VN9F2KcRzVa1} and {Link|LinkedIn profile|url=https://www.linkedin.com/in/maarten-zwaal-b1aa16a3/}

| Header
    Iris Bondoc

Student of Business Informatics, Utrecht University

See {Link|Iris's program|url=https://ellie-app.com/4VNvz7HvNcCa1}

| Header
    Daniel Kirk

Student of Biology of Disease, Utrecht University

See {Link|Dan's program|url=https://ellie-app.com/4XBfc9d87RLa1}


| Header
    Ishak Guelai

Student of Pharmaceutical Sciences, Utrecht University

See {Link|Ishak's program|url=https://ellie-app.com/4X6QQjNV3xVa1}

| Header
    Anouck Fietje

Student of Psychology, Utrecht University

See {Link|Anouck's program|url=https://ellie-app.com/4Yq4PR47CD5a1}

| Emphasize
    We are hoping to do it again.

    If you are interested please get in touch


    {Icon|name=phone} {Link|+31 638 216 166|url=tel:+31638216166} | {Icon|name=mail} {Link|software-garden@tad-lispy.com|url= mailto:software-garden@tad-lispy.com} | {Icon|name=home} {Link|Back|url=/}
